/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform . All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of .
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with .
 * 
 * Modified history:
 *   baibai  2021年4月16日 下午11:29:04  created
 */
package com.desktop.web.service.websession;

import java.util.Date;

/**
 * 
 *
 * @author baibai
 */
public class WebSession {

    private Long uid;
    private String username;
    private String token;
    private Date lastTime;

    public WebSession(Long uid, String username, String token) {
        this.uid = uid;
        this.token = token;
        this.username = username;
    }

    /**
     * @return the uid
     */
    public Long getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the lastTime
     */
    public Date getLastTime() {
        return lastTime;
    }

    /**
     * @param lastTime the lastTime to set
     */
    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

}
