/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform baibai. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of baibai.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with baibai.
 * 
 * Modified history:
 *   baibai  2020年4月17日 下午10:48:24  created
 */
package com.desktop.web.service.log;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.service.user.UserService;
import com.desktop.web.uda.entity.Log;
import com.desktop.web.uda.entity.User;
import com.desktop.web.uda.mapper.LogMapper;

/**
 * 
 *
 * @author baibai
 */
@Service
public class LogService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LogMapper logMapper;

    @Autowired
    private UserService userService;

    /**
     * 添加log
     * 
     * @param user
     * @param module
     * @param content
     */
    public void addLog(Long uid, String module, String content, Object... params) {
        User user = userService.getUserById(uid);
        if (user == null) {
            throw new BusinessException("用户找不到");
        }

        this.addLog(user, module, content, params);
    }

    /**
     * 添加log，需要有用户上下文
     * 
     * @param module
     * @param content
     * @param params
     */
    public void addLogCuruser(String module, String content, Object... params) {

        User user = RequestUtil.getUser();
        content = content.replace("{}", "%s");
        content = String.format(content, params);

        Log entity = new Log();
        entity.setUid(user.getId());
        entity.setUsername(user.getUsername());
        entity.setModule(module);
        entity.setContent(content);
        entity.setCtime(new Date());
        logMapper.insert(entity);

        logger.debug("username:{},uid:{}, module:{},content:{}", user.getUsername(), user.getId(), module, content);
    }

    /**
     * 添加log
     * 
     * @param user
     * @param module
     * @param content
     */
    public void addLog(User user, String module, String content, Object... params) {

        content = content.replace("{}", "%s");
        content = String.format(content, params);

        Log entity = new Log();
        entity.setUid(user.getId());
        entity.setUsername(user.getUsername());
        entity.setModule(module);
        entity.setContent(content);
        entity.setCtime(new Date());
        logMapper.insert(entity);

        logger.debug("username:{},uid:{}, module:{},content:{}", user.getUsername(), user.getId(), module, content);
    }
}
