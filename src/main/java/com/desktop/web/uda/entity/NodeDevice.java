/**
 * 
 */
package com.desktop.web.uda.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.desktop.web.core.db.BaseEntity;

/**
 * @author baibai
 *
 */
@TableName("t_node_device")
public class NodeDevice extends BaseEntity {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    private Long nodeId;
    private Long deviceId;

    /**
     * @return the nodeId
     */
    public Long getNodeId() {
        return nodeId;
    }

    /**
     * @param nodeId the nodeId to set
     */
    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * @return the deviceId
     */
    public Long getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

}
