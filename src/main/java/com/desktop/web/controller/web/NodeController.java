/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月2日 下午11:58:03  created
 */
package com.desktop.web.controller.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.desktop.web.core.aop.RightTarget;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.core.utils.Util;
import com.desktop.web.core.web.Result;
import com.desktop.web.service.node.NodeService;
import com.desktop.web.uda.entity.Node;

/**
 * 
 *
 * @author baibai
 */
@Controller
public class NodeController extends BaseWebController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private NodeService nodeService;

    @RequestMapping(value = "/webapi/device/node/tree", method = {RequestMethod.GET})
    @ResponseBody
    @RightTarget(value = "device_node_select,team_role_manager")
    public Object getNodeTree(HttpServletRequest request) {
        try {
            Object tree = nodeService.getNodeTreeCuruser();
            return Result.Success(tree);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/device/node/add", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_node_manager")
    public Object addNode(HttpServletRequest request) {
        try {

            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }
            Long pid = Long.valueOf(params.get("pid"));
            nodeService.checkNodeByCuruser(pid);
            Node node = nodeService.addNode(pid, params.get("title"));
            return Result.Success(node);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/device/node/rename", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_node_manager")
    public Object renameNode(HttpServletRequest request) {
        try {

            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            Long id = Long.valueOf(params.get("id"));
            nodeService.checkNodeByCuruser(id);
            nodeService.renameNode(id, params.get("title"));
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/device/node/delete", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_node_manager")
    public Object deleteNode(HttpServletRequest request) {
        try {

            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }
            Long id = Long.valueOf(params.get("id"));
            nodeService.checkNodeByCuruser(id);
            nodeService.deleteNode(id);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/device/node/move", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "device_node_manager")
    public Object moveNode(HttpServletRequest request) {
        try {

            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            Long nodeId = Long.valueOf(params.get("id"));
            Long targetId = Long.valueOf(params.get("targetId"));
            List<Long> sortIds = Util.toArrayLong(params.get("list").toString());

            nodeService.checkNodeByCuruser(nodeId);
            nodeService.checkNodeByCuruser(targetId);
            nodeService.moveNode(nodeId, targetId, sortIds);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

}
